__author__ = 'Andra Waagmeester'

from bs4 import BeautifulSoup
import requests
from SPARQLWrapper import SPARQLWrapper, JSON
import pprint
import sys

'''
# login to wikipedia
payload = {
    'wpName1': 'andrawaag',
    'wpPassword1': 'nijntje1'
}

with requests.Session() as s:
    p = s.post('https://www.wikidata.org/w/index.php?title=Special:UserLogin&returnto=Wikidata%3AMain+Page', data=payload)
    print(p.text)
'''

sparql = SPARQLWrapper("https://query.wikidata.org/bigdata/namespace/wdq/sparql")

sparql.setQuery("""
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wd: <http://www.wikidata.org/entity/>
SELECT * WHERE {
  ?gene wdt:P279 wd:Q7187 .
  ?gene wdt:P279 wd:Q8054 .
}
""")
sparql.setReturnFormat(JSON)
results = sparql.query().convert()

pprint.pprint(results)
file = open("/tmp/proteingene.html", "w")
file.write("""
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
</head>
  <body>
    <table>
""")
for result in results["results"]["bindings"]:
    mergedId = result["gene"]["value"].replace("http://www.wikidata.org/entity/", '')
    html_doc = requests.get("https://www.wikidata.org/w/index.php?title="+mergedId+"&action=history")
    soup = BeautifulSoup(html_doc.text, 'html.parser')
    otherId = None
    for link in soup.find_all('li'):
        for span in link.find_all('span'):
            #print(type(span))
            if "Merged" in str(span.string):
                otherId = str(span.string)
                otherId = otherId.split()[len(otherId.split())-1]
                continue
    #print(otherId)
    sourceId = otherId
    html1 = "https://www.wikidata.org/w/index.php?title="+mergedId+"&action=history"
    html2 = "https://www.wikidata.org/w/index.php?title="+sourceId+"&action=history"
    print(mergedId + " - "+sourceId)
    print(html1 + " - "+html2)
    file.write("<tr><td><div id='"+mergedId+"'></div></td><td><div id='"+sourceId+"'></div></td></tr>"
               "<script type='text/javascript'>$('#"+mergedId+"').load('"+html1+"');</script><script type='text/javascript'>$('#"+sourceId+"').load('"+html2+"');</script>" +
               "<script>var value = $(\"#"+sourceId+"\").val();value = value.replace(\"/w/index.php?title\", \"https://www.wikidata.org/w/index.php?title\");" +
               "$(\"#"+sourceId+"\").val(value)</script>")

    #print("https://www.wikidata.org/w/index.php?title="+mergedId+"&action=history")
    #print("https://www.wikidata.org/w/index.php?title="+sourceId+"&action=history")
file.write("""
    </table>
  </body>
</html>
""")
file.close()